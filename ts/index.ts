import * as plugins from './smartfile.plugins.js';
import * as fsMod from './smartfile.fs.js';
import * as fsStreamMod from './smartfile.fsstream.js';
import * as interpreterMod from './smartfile.interpreter.js';
import * as memoryMod from './smartfile.memory.js';

export * from './smartfile.classes.smartfile.js';
export * from './smartfile.classes.virtualdirectory.js';

export const fs = fsMod;
export const fsStream = fsStreamMod;
export const interpreter = interpreterMod;
export const memory = memoryMod;
